import { Router } from "https://deno.land/x/oak/mod.ts";
import * as planets from "./models/planets.ts";
import * as launches from "./launches.ts";
const router = new Router();

router.get("/", (ctx) => {
  ctx.response.body = `
        ___ _ 
    /_\    / _ (_)
   //_\\  / /_)/ |
  /  _  \/ ___/| |
  \_/ \_/\/    |_|
   `;
  });

  router.get("/planets", (ctx) => {
    ctx.response.body = planets.getAll();
  });
  
  router.get("/launches", (ctx) => {
    ctx.response.body = launches.getAll();
  });
  
  router.get("/launches/:id", (ctx) => {
    if (ctx.params?.id) {
      const launchData = launches.getOne(Number(ctx.params.id));
      if (launchData) {
        ctx.response.body = launchData;
      } else {
        ctx.throw(400, "Launch with that ID doesn't exist");
      }
    }
  });
  
  router.delete("/launches/:id", (ctx) => {
    if (ctx.params?.id) {
      const result = launches.removeOne(Number(ctx.params.id));
      ctx.response.body = { success: result };
    }
  });
  
  router.post("/launches", async (ctx) => {
    const body = await ctx.request.body();
  
    launches.addOne(body.value);
  
    ctx.response.body = { success: true };
    ctx.response.status = 201;
  });
  
  export default router;