import { Application, send } from "https://deno.land/x/oak/mod.ts";
import  * as log from "https://deno.land/std@0.57.0/log/mod.ts";
import api from "./api.ts";
const app = new Application();
const PORT = 3000;

app.use(async (ctx, next) => {
  await next();
  const time = ctx.response.headers.get("X-Response-Time");
  console.log(`${ctx.request.method} ${ctx.request.url}: ${time}`);
});

app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const end = Date.now() - start;
  ctx.response.headers.set("X-Response-Time", `${end}ms`);
});

app.use(api.routes());

app.use(async (ctx) => {
  const path = ctx.request.url.pathname;
  const whiteList = [
    "/index.html",
    "/main.js",
    "/style.css",
    "/images/favicon.png",
  ];
  if (whiteList.includes(path)) {
    await send(ctx, path, {
      root: "./public",
    });
  }
});

if (import.meta.main) {
  await app.listen({
    port: PORT,
  });
}
